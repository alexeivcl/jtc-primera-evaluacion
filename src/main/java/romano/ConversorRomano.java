package romano;

/**
 * Clase ConversorRomano
 * <br/>Curso de Programacion Java
 * @author Derlis Zarate
 */
public class ConversorRomano {
    
    /**
     * Convierte un numero decimal a romano.
     * Funciona para nros. entre 1 y 3999
     * @param i Nro Decimal a convertir
     * @return Representacion del nro. decimal en romano
     * @throws NroInvalidoException Si el nro no esta en el rango 1-3999
     */
    public static String decimal_a_romano(int i) throws NroInvalidoException
    {
        if (i < 1 || i > 3999) {
            throw new NroInvalidoException();
        }
        
        String R = "";

        while( (i/1000) >= 1 ) { R  += "M"; i -= 1000; }

        if (i/900>=1) { R += "CM"; i-=900; }
        if (i/500>=1) { R += "D"; i-=500; }
        if (i/400>=1) { R += "CD"; i-=400; }

        while ( i/100>=1 ) { R += "C"; i-=100; }

        if ( i/90>=1 ) { R += "XC"; i-=90; }
        if ( i/50>=1 ) { R += "L"; i-=50; }
        if ( i/40>=1 ) { R += "XL"; i-=40; }

        while ( i/10>=1 ) { R +="X"; i-=10; }

        if ( i/9>=1 ) { R +="IX"; i-=9; }
        if ( i/5>=1 ) { R +="V"; i-=5; }
        if ( i/4>=1 ) { R +="IV"; i-=4; }
        while ( i>=1 ) { R +="I"; i-=1; }

        return R;
    }

} 
