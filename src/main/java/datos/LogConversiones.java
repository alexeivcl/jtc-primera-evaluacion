package datos;
import java.util.Date;

/**
 * Clase LogConversiones. Representa a 1 registro de la tabla LOG_CONVERSIONES
 * Curso de Programacion Java
 * @author Alexei Perez Hurtado
 */
public class LogConversiones {
    
	private Integer ID_MENSAJE = 0;
    private String NOMBRE_THREAD = null;
    private String IP_CLIENTE = null;
    private Date FECHA_HORA = null;
    private Integer MSG_REQUEST = 0;
    private String MSG_RESPONSE = null;
    
    
	/**
	 * @param iD_MENSAJE
	 * @param nOMBRE_THREAD
	 * @param iP_CLIENTE
	 * @param fECHA_HORA
	 * @param mSG_REQUEST
	 * @param mSG_RESPONSE
	 */
	public LogConversiones(String nOMBRE_THREAD, String iP_CLIENTE, Date fECHA_HORA,
			Integer mSG_REQUEST, String mSG_RESPONSE) {
		super();
		
		NOMBRE_THREAD = nOMBRE_THREAD;
		IP_CLIENTE = iP_CLIENTE;
		FECHA_HORA = fECHA_HORA;
		MSG_REQUEST = mSG_REQUEST;
		MSG_RESPONSE = mSG_RESPONSE;
	}


	/**
	 * 
	 */
	public LogConversiones() {
		super();		
	}
	
	/**
	 * @return the iD_MENSAJE
	 */
	public Integer getID_MENSAJE() {
		return ID_MENSAJE;
	}


	/**
	 * @param iD_MENSAJE the iD_MENSAJE to set
	 */
	public void setID_MENSAJE(Integer iD_MENSAJE) {
		ID_MENSAJE = iD_MENSAJE;
	}


	/**
	 * @return the nOMBRE_THREAD
	 */
	public String getNOMBRE_THREAD() {
		return NOMBRE_THREAD;
	}


	/**
	 * @param nOMBRE_THREAD the nOMBRE_THREAD to set
	 */
	public void setNOMBRE_THREAD(String nOMBRE_THREAD) {
		NOMBRE_THREAD = nOMBRE_THREAD;
	}


	/**
	 * @return the iP_CLIENTE
	 */
	public String getIP_CLIENTE() {
		return IP_CLIENTE;
	}


	/**
	 * @param iP_CLIENTE the iP_CLIENTE to set
	 */
	public void setIP_CLIENTE(String iP_CLIENTE) {
		IP_CLIENTE = iP_CLIENTE;
	}


	/**
	 * @return the fECHA_HORA
	 */
	public Date getFECHA_HORA() {
		return FECHA_HORA;
	}


	/**
	 * @param fECHA_HORA the fECHA_HORA to set
	 */
	public void setFECHA_HORA(Date fECHA_HORA) {
		FECHA_HORA = fECHA_HORA;
	}


	/**
	 * @return the mSG_REQUEST
	 */
	public Integer getMSG_REQUEST() {
		return MSG_REQUEST;
	}


	/**
	 * @param mSG_REQUEST the mSG_REQUEST to set
	 */
	public void setMSG_REQUEST(Integer mSG_REQUEST) {
		MSG_REQUEST = mSG_REQUEST;
	}


	/**
	 * @return the mSG_RESPONSE
	 */
	public String getMSG_RESPONSE() {
		return MSG_RESPONSE;
	}


	/**
	 * @param mSG_RESPONSE the mSG_RESPONSE to set
	 */
	public void setMSG_RESPONSE(String mSG_RESPONSE) {
		MSG_RESPONSE = mSG_RESPONSE;
	}	
    
} 
