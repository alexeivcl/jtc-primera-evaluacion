package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import utils.LogUtil;
import java.util.ArrayList;
import java.util.List;

import datos.LogConversiones;
import datos.LogConversionesManager;
import romano.ConversorRomano;
import romano.NroInvalidoException;



/**
 * Clase ConversorRomano Thread encargada de procesar toda la logica de cada cliente que se comunica con el Server Socket.
 * Permite que el servidor pueda atender mas de un cliente al mismo tiempo.
 * @author Alexei Perez Hurtado
 *
 */
public class ConversorRomanoThread extends Thread {    
	
    private Socket cliente;
    private ServerSocket server = null;
    private LogConversionesManager logManager = new LogConversionesManager();
    LogConversiones log = null;
    String hilo = null;
    String ip = null;
    Date fechaHora = null;
    Integer numeroDec = 0;
    String msgRespone = null;
       
    
    public ConversorRomanoThread(Socket cliente, ServerSocket server) {
        this.cliente = cliente;
        this.server = server;
    }
    
    @Override
    public void run() {
    	LogUtil.INFO("Nueva peticion recibida...");
    	
    	try { 
    		
    		OutputStream clientOut = cliente.getOutputStream();
    		PrintWriter pw = new PrintWriter(clientOut, true);
    		
    		try {
    			StartMenu(pw);
			} catch (Exception e) {
				pw.println("Posibles problemas con la informacion del cliente socket." + e.getMessage());
			}
    		
    		
            InputStream clientIn = cliente.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
            
            String mensajeRecibido = br.readLine();  
           
            if (mensajeRecibido != null) {
            	if (mensajeRecibido.trim().equalsIgnoreCase("fin")) {
            		LogUtil.INFO("El servidor se apagado por comando del cliente " + GetThreadName() + " desde el IP " + GetIpAddress(cliente).split("/")[1]);
            		pw.println("El servidor se esta apagando...");
            		this.server.close();            		 
            	} 
            	
            	else if (mensajeRecibido.trim().startsWith("conv")) {		             		             
		             String strnum = mensajeRecibido.substring(4);
		             int numero = Integer.parseInt(strnum.trim());
		             
		             CrearLogConversiones(strnum, numero);
		             InsertLogConversiones(log);     
		             
		             LogUtil.INFO("Se guardo un nuevo Log por comando del cliente " + GetThreadName() + " desde el IP " + GetIpAddress(cliente).split("/")[1]);
		             pw.println("La conversion a numero romano se realizo correctamente.");
		             pw.println("Resultado de la operacion: ");
		             pw.println("Nro Decimal de Entrada: " + log.getMSG_REQUEST() + " Numero Romano Convertido: " + log.getMSG_RESPONSE());
            	}
            	
            	else if (mensajeRecibido.trim().startsWith("find")) {		             		             
		             String strnum = mensajeRecibido.substring(4);
		             int numero = Integer.parseInt(strnum.trim());
		             
		             log = FindLogConversionesByID(numero);
		             if(log != null){
		            	 pw.println("\n");
		            	 pw.println("Datos del Log: ");
		            	 pw.println("IP: " + log.getIP_CLIENTE().toString() + " Holi: " + log.getNOMBRE_THREAD().toString() + " IP Cliente: " + log.getIP_CLIENTE().toString() + " Fecha: " + log.getFECHA_HORA().toString() );
		            	 
		             }else{
		            	 pw.println("No existe un log con ese ID."); 
		             }
		             
            	}
            	
            	else if (mensajeRecibido.trim().startsWith("list")) {		             		             
		              
            		List<LogConversiones> logs = new ArrayList<LogConversiones>();            		
            		logs = GetAllLogsConversiones();
            				             		             
		            if(logs != null && logs.size() > 0){
		            	pw.println("Hay " + logs.size() + " Logs almacenados en BD.");
		            	for(int i=0; i<logs.size();i++){
		            		pw.println("Log Nro: " + logs.get(i).getID_MENSAJE());
		            		pw.println("IP: " + logs.get(i).getIP_CLIENTE().toString() + " Holi: " + logs.get(i).getNOMBRE_THREAD().toString() + " IP Cliente: " + logs.get(i).getIP_CLIENTE().toString() + " Fecha: " + logs.get(i).getFECHA_HORA().toString() );
		            		
		            	}		            	
		            }else{
		            	pw.println("No existen logs en BD."); 
		            }		             
            	}
            	
            	else if (mensajeRecibido.trim().startsWith("del")) {
            		
		            try {		            	
		            	
		            	if (mensajeRecibido.substring(4) != "" && mensajeRecibido.substring(4) != null) {
		            		
		            		String strnum = mensajeRecibido.substring(4);
				            int numero = Integer.parseInt(strnum.trim());
		            		
		            		DeleteLogConversiones(numero);
		            		LogUtil.INFO("Se elimino el Log nro " + numero + " por el cliente " + GetThreadName() + " desde el IP " + GetIpAddress(cliente).split("/")[1]);
			            	pw.println("El log se ha eliminado correctamente.");
						}else{
			            	pw.println("Debe entrar un ID para eliminar un Log."); 
			            }		            	
					} catch (Exception e) {
						pw.println("El formato de la cadena no es correcto. " + e.getMessage());
					}   	
		           		             
            	}
            	
            	else if(!mensajeRecibido.trim().startsWith("conv") && !mensajeRecibido.trim().startsWith("list") && !mensajeRecibido.trim().startsWith("find") && !mensajeRecibido.trim().startsWith("del")){
            		pw.println("Debe seleccionar una operacion valida. Por favor intente de nuevo");
            	}
            	
            } 
            else {
            	pw.println("Mensaje recibido no valido.. reintente por favor!!");
            }                        
            
    		try {
    			System.out.println("Cerrando conexion del socket... ");
    			pw.println("Cerrando el socket del cliente " + GetThreadName() +" ...");
				Thread.sleep(3000);
			} catch (InterruptedException e) {				
				e.printStackTrace();
			}
    		
            clientIn.close();
    		clientOut.close();
    		cliente.close();
    		LogUtil.INFO("Finalizando atencion de la peticion recibida...");
			
		} catch (IOException e) {			
			e.printStackTrace();
		} catch (NumberFormatException e1) {			
			e1.printStackTrace();
		}       
    }
    
    
    /**
     * Metodo que recibe un Socket cliente y devuelve sus datos de host remotos.
     * Devuelve una cadena que contiene el nombre de host y la direccion IP
     * @param client
     * @return String
     * @throws UnknownHostException
     */
    private String GetIpAddress(Socket client) throws UnknownHostException{		
    	client.getInetAddress();
		String ipAddress = InetAddress.getLocalHost().toString();		
		
		return ipAddress;    	
    }
    
    /**
     * Metodo que devuelve el nombre del Thread que se esta ejecutando.
     * @return String
     */
    private String GetThreadName(){
    	
    	return this.getName().toString();
    }
    
    /**
     * Metodo que devuelve la fecha en el momento que es consultada en tipo Date.
     * Usado principalmente para saber el momento exacto que se guarda el Log.
     * @return Date
     */
    private Date CurrentDateTime(){
    	
    	Date date = new Date();
    	return date; 
    }
    
    /**
     * Metodo que sabe como crear un Log y recibe como parametros el numero a convertir como una cadena,
     * y el mismo numero como un entero para guardarlos en funcion del tipo de datos en la BD.
     * @param mmsgRec
     * @param numConv
     * @return LogConversiones
     */
    private LogConversiones CrearLogConversiones(String mmsgRec, Integer numConv){
    	
    	String romano = null;
		try {
			romano = ConversorRomano.decimal_a_romano(numConv);
		} catch (NumberFormatException | NroInvalidoException e) {
			
			e.printStackTrace();
		}
    	
    	String hilo = GetThreadName();
        String ip = null;
		try {
			
			ip = GetIpAddress(cliente).split("/")[1];
			
		} catch (UnknownHostException e) {
			
			e.printStackTrace();
		}
		
        Date fechaHora = CurrentDateTime();
        Integer numeroDec = numConv;
        String msgRespone = romano;
        
        log = new LogConversiones(hilo, ip, (java.util.Date)fechaHora, numeroDec, msgRespone);
        
        return log;
    }
    
    /**
     * Metodo que sabe como insertar en BD un Log luego de haberlo procesado
     * @param LogConversiones log
     */
    private void InsertLogConversiones(LogConversiones log){
    	
    	logManager.insertarNuevoRegistro(log);
    }
    
    /**
     * Metodo que sabe como encontrar en BD un Log determinado por el ID.
     * Retorna el Log con todas sus propiedades.
     * @param numConv
     * @return LogConversiones
     */
    private LogConversiones FindLogConversionesByID(Integer numConv){
    	
    	LogConversiones logFind = logManager.buscarPorID(numConv);
    	return logFind;
    }
    
    /**
     * Metodo que sabe como devolver una lista de Logs desde la BD
     * @return List<LogConversiones>
     */
    private List<LogConversiones> GetAllLogsConversiones(){
    	
    	List<LogConversiones> logs = logManager.getAll();
    	return logs;
    }
    
    /**
     * Metodo que sabe como eliminar un Log de la BD
     * @param IdLog
     */
    private void DeleteLogConversiones(int IdLog){
    	
    	logManager.eliminarRegistro(IdLog);
    	
    }
    
    /**
     * Metodo que se encarga de mostrar un menu de opciones para el cliente.
     * Puede lanzar un excepcion en caso de que haya problemas con la informacion remota del cliente
     * @param pw
     * @throws UnknownHostException
     */
    private void StartMenu(PrintWriter pw) throws UnknownHostException{
    	System.out.println("Cliente conectado " + GetThreadName()+ " desde el IP " + GetIpAddress(cliente).split("/")[1]);
    	System.out.println("Seleccione una de las siguientes opciones:");
    	System.out.println("conv numDecimal ----- Para convertir decimal a numero romano");
    	System.out.println("find idLog ---------- Para encontrar un Log en BD");
    	System.out.println("list ---------------- Para ver todos los Logs que hay en BD");
    	System.out.println("del numDecimal ------ Para eliminar un Log en BD");
    	System.out.println("fin ----------------- Para cerrar el proceso del Servidor Socket");
    	
    }
}
