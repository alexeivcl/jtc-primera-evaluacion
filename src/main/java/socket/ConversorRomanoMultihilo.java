package socket;

import java.net.*;
import java.util.Random;
import java.io.*;
import utils.LogUtil;

/**
 * Clase ConversorRomanoServer Multihilo
 * Curso de Programacion Java
 * @author Alexei Perez Hurtado
 */
public class ConversorRomanoMultihilo {

    public static void main(String[] args) throws IOException {
        ServerSocket servidor = null;
        Socket cliente;
        
        try {
            
        	LogUtil.INFO("Iniciando socket server en puerto 6400");
            servidor = new ServerSocket(6400);
            System.out.println("Servidor Socket Iniciado... ");
            
        } catch (IOException ie) {
            System.err.println("Error al abrir socket. " + ie.getMessage());
            System.exit(1);
        }
        
        LogUtil.INFO("Socket iniciado, se atienden peticiones..");
                
        while (true) {
            try {
            	if (servidor.isClosed()) {
            		System.out.println("El servidor no esta corriendo. ");
            		break;
				} else {
					cliente = servidor.accept();
	                ConversorRomanoThread crt = new ConversorRomanoThread(cliente, servidor);
	                cliente.getInetAddress();
	                String ipAddress = InetAddress.getLocalHost().toString();
	                Random rand = new Random();
					crt.setName("Thread-"+ (rand.nextInt()));
					LogUtil.INFO("Nuevo cliente " + crt.getName() + " conectado al servidor desde " + ipAddress.split("/")[1]);
	                crt.start();  

				}                                 
                
            } catch (IOException ie) {
                System.out.println("Error al procesar socket. " + ie.getMessage());
                servidor.close();
            }
        }        
    }
}